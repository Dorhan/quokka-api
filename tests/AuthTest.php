<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class AuthTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Try to get a token.
     *
     * @return string
     */
    public function testAskToken() : string
    {
        // Create user in database
        $user = factory(App\Models\User::class)->make();
        $user->save();

        // Ask a token for a dummy user
        $this->json('GET', env('API_DOMAIN').'/auth/ask-token/dummy_user@quokka.com')
            ->seeJson([
                'status_code' => 404,
            ])
            ->seeStatusCode(404);

        // Ask a token for the real user
        $data = $this->json('GET', env('API_DOMAIN').'/auth/ask-token/'.$user->email)->seeStatusCode(200);
        $data = json_decode($data->response->getContent());
        $this->assertArrayHasKey('token', (array) $data);

        return $data->token;
    }

    /**
     * Try to use a token.
     *
     * @return void
     */
    public function testUseToken()
    {
        // Use an invalid token
        $this->call('GET', env('API_DOMAIN').'/auth/use-token/abcd');
        $this->assertResponseStatus(404);

        // Create user in database
        $token = $this->testAskToken();

        // Use the right token
        $data = $this->json('GET', env('API_DOMAIN').'/auth/use-token/'.$token)->seeStatusCode(200);
        $data = json_decode($data->response->getContent());
        $this->assertArrayHasKey('token', (array) $data);
        $this->assertArrayHasKey('user', (array) $data);
    }
}
