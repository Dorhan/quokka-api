<?php

abstract class TestCase extends Laravel\Lumen\Testing\TestCase
{
    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        return require __DIR__.'/../bootstrap/app.php';
    }

    /**
     * Create a new user and return his token
     *
     * @return string The authentication token
     */
    protected function createAndLogUser() : string
    {
        $user = factory(App\Models\User::class)->make();
        $user->save();

        return $this->logUser($user);
    }

    /**
     * Create a new user and return his token
     *
     * @param App\Models\User $user A User instance
     * @return string The authentication token
     */
    protected function logUser(App\Models\User $user) : string
    {
        // Ask a token
        $data = $this->json('GET', env('API_DOMAIN').'/auth/ask-token/'.$user->email);
        $data = json_decode($data->response->getContent());

        // Use it
        $data = $this->json('GET', env('API_DOMAIN').'/auth/use-token/'.$data->token);
        $data = json_decode($data->response->getContent());

        return $data->token;
    }
}
