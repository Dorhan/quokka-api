<?php

namespace App\Api\V1\Controllers;

use App\Api\V1\Transformers\UserTransformer;
use App\Events\UserCreatedEvent;
use App\Models\User;
use DB;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;

/**
 * User resource representation.
 *
 * @Resource("Users", uri="/users")
 */
class UsersController extends BaseController
{
    /**
     * Retrieve all users (paginated)
     *
     * @param int $perPage User count to return
     * @return json
     */
    public function all(int $perPage = 15)
    {
        $users = User::paginate($perPage);
        return $this->response->paginator($users, new UserTransformer);
    }

    /**
     * Create a new user
     *
     * @param Request $request Request data
     * @return json
     * @throws StoreResourceFailedException
     */
    public function create(Request $request)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), User::rules());
        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new user.', $validator->errors());
        }

        // Create user
        $user = User::create([
            'email' => $request->get('email'),
            'name' => $request->get('name')
        ]);

        // Send an event to notify about the new user
        Event::fire(new UserCreatedEvent());

        return $this->response->item($user, new UserTransformer)->setStatusCode(201);
    }

    /**
     * Remove users with the given identifiers
     *
     * @param string $users_id Identifiers
     * @return int User count removed
     */
    public function remove(string $users_id)
    {
        return User::destroy(explode(',', $users_id));
    }

    /**
     * Search in users
     *
     * @param Request $request Request data
     * @return json
     */
    public function search(Request $request)
    {
        $users = new User();

        if ($request->has('name'))
            $users = $users->where('name', 'LIKE', '%'.$request->get('name').'%');

        if ($request->has('email'))
            $users = $users->where('email', 'LIKE', '%'.$request->get('email').'%');

        return $this->response->collection($users->get(), new UserTransformer);
    }

    /**
     * Retrieve users with the given identifiers
     *
     * @param string $users_id Identifiers
     * @return json
     */
    public function show(string $users_id)
    {
        $users_id = explode(',', $users_id);
        $users = User::whereIn('id', $users_id)->get();

        if (count($users) != count($users_id))
        {
            // Retrieve missing identifiers to notify the user
            $result_ids = collect($users)->pluck('id')->toArray();
            $diffs = array_diff($users_id, $result_ids);

            return $this->response->errorNotFound('User(s) not found (Id: '.implode(', ', $diffs).')');
        }

        return $this->response->collection($users, new UserTransformer);
    }

    /**
     * Retrieve organization's users
     *
     * @param string $organization_id Owner identifier
     * @return json
     */
    public function showForOrganization(string $organization_id)
    {
        $users = User::whereHas('organizations', function($query) use($organization_id)
                    {
                        $query->where('id', '=', $organization_id);
                    })->get();

        return $this->response->collection($users, new UserTransformer);
    }

    /**
     * Update the given user
     *
     * @param Request $request Request data
     * @param string $user_id Identifier
     * @return json
     * @throws UpdateResourceFailedException
     */
    public function update(Request $request, string $user_id)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), User::rules($user_id));
        if ($validator->fails()) {
            throw new UpdateResourceFailedException('Could not update user.', $validator->errors());
        }

        // Update data
        $user = User::findOrFail($user_id)->first();
        $user->email = $request->has('email') ? $request->get('email') : $user->email;
        $user->name = $request->has('name') ? $request->get('name') : $user->name;
        $user->save();

        return $this->response->item($user, new UserTransformer);
    }
}
