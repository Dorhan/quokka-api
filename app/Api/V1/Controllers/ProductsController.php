<?php

namespace App\Api\V1\Controllers;

use DB;
use App\Api\V1\Transformers\ProductTransformer;
use App\Models\Product;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;

/**
 * Product resource representation.
 *
 * @Resource("Products", uri="/products")
 */
class ProductsController extends BaseController
{
    /**
     * Retrieve all products (paginated)
     *
     * @param int $perPage Product count to return
     * @return json
     */
    public function all(int $perPage = 15)
    {
        $products = Product::paginate($perPage);
        return $this->response->paginator($products, new ProductTransformer);
    }

    /**
     * Create a new product
     *
     * @param Request $request Request data
     * @param string $organization_id Oranization owning the product
     * @return json
     * @throws StoreResourceFailedException
     */
    public function create(Request $request, string $organization_id)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), Product::rules());
        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new product.', $validator->errors());
        }

        // Create the product
        $product = Product::create([
            'name' => $request->get('name'),
            'organization_id' => $organization_id
        ]);

        return $this->response->item($product, new ProductTransformer)->setStatusCode(201);
    }

    /**
     * Remove products with the given identifiers
     *
     * @param string $products_id Identifiers
     * @return int Product count removed
     */
    public function remove(string $products_id)
    {
        return Product::destroy(explode(',', $products_id));
    }

    /**
     * Retrieve products with the given identifiers
     *
     * @param string $products_id Identifiers
     * @return json
     */
    public function show(string $products_id)
    {
        $products_id = explode(',', $products_id);
        $products = Product::whereIn('id', $products_id)->get();

        if (count($products) != count($products_id))
        {
            // Retrieve missing identifiers to notify the product
            $result_ids = collect($products)->pluck('id')->toArray();
            $diffs = array_diff($products_id, $result_ids);

            return $this->response->errorNotFound('Product(s) not found (Id: '.implode(', ', $diffs).')');
        }

        return $this->response->collection($products, new ProductTransformer);
    }

    /**
     * Retrieve organization's products
     *
     * @param string $organization_id Owner identifier
     * @return json
     */
    public function showForOrganization(string $organization_id)
    {
        $products = Product::where('organization_id', $organization_id)->get();
        return $this->response->collection($products, new ProductTransformer);
    }

    /**
     * Update the given product
     *
     * @param Request $request Request data
     * @param string $product_id Identifier
     * @return json
     * @throws UpdateResourceFailedException
     */
    public function update(Request $request, string $product_id)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), Product::rules());
        if ($validator->fails()) {
            throw new UpdateResourceFailedException('Could not update product.', $validator->errors());
        }

        // Update data
        $product = Product::findOrFail($product_id)->first();
        $product->name = $request->has('name') ? $request->get('name') : $product->name;
        $product->description = $request->has('description') ? $request->get('description') : $product->description;
        $product->website = $request->has('website') ? $request->get('website') : $product->website;
        $product->save();

        return $this->response->item($product, new ProductTransformer);
    }
}
