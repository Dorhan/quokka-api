<?php

namespace App\Api\V1\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\UserToken;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

/**
 * AuthenticateController.
 */
class AuthenticateController extends BaseController
{
    /**
     * Return a new token
     *
     * @param string $user_email User's email
     * @return json
     */
    public function askToken(string $user_email)
    {
        $user = User::where('email', $user_email)->first();
        if (!$user)
            return $this->response->errorNotFound();

        $user_token = UserToken::where('user_id', $user->id)->first();
        if (!$user_token)
            $user_token = new UserToken();

        $user_token->user_id = $user->id;
        $user_token->token = UserToken::generateToken();
        $user_token->save();

        return $this->response->array(['token' => $user_token->token]);
    }

    /**
     * Use a token
     *
     * @param JWTAuth $auth JWTAuth instance
     * @param string $token User's token
     * @return json
     */
    public function useToken(JWTAuth $auth, string $token)
    {
        // Search the valid token …
        $user_token = UserToken::where('token', $token)->active()->with('user')->first();
        if (!$user_token)
            return response('Invalid token', 404);

        // … then invalidate it.
        $user_token->updated_at = Carbon::now()->subMonth(1);
        $user_token->save(['timestamps' => false]);

         // Try to identify the user
        try 
        {
            $token = $auth->fromUser($user_token->user);
        } 
        catch (JWTException $e) 
        {
            return $this->response->array(['error' => 'could_not_create_token']);
        }
            
        // Invalidate user
        if (!$token)
            return $this->response->array(['error' => 'invalid_credentials']);

        return $this->response->array(['user' => $user_token->user, 'token' => $token]);
    }

    /**
     * Refresh a token
     *
     * @param JWTAuth $auth JWTAuth instance
     * @param string $token User's token
     * @return json
     */
    public function refreshToken(JWTAuth $auth, string $token)
    {
        $new_token = $auth->refresh($auth->getToken());
        return $this->response->array(['token' => $new_token]);
    }
}

