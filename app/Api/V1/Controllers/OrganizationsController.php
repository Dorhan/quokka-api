<?php

namespace App\Api\V1\Controllers;

use DB;
use App\Api\V1\Transformers\OrganizationTransformer;
use App\Models\Organization;
use Dingo\Api\Exception\StoreResourceFailedException;
use Dingo\Api\Exception\UpdateResourceFailedException;
use Illuminate\Http\Request;

/**
 * Organization resource representation.
 *
 * @Resource("Organizations", uri="/organizations")
 */
class OrganizationsController extends BaseController
{
    /**
     * Retrieve all organizations (paginated)
     *
     * @param int $perPage Organization count to return
     * @return json
     */
    public function all(int $perPage = 15)
    {
        $organizations = Organization::paginate($perPage);
        return $this->response->paginator($organizations, new OrganizationTransformer);
    }

    /**
     * Add users to an organization
     *
     * @param Request $request Request data
     * @param string $organization_id Organization's identifier
     * @param string $users_id Identifiers
     * @return json
     */
    public function attachUsers(Request $request, string $organization_id, string $users_id)
    {
        // Find user
        $organization = Organization::find($organization_id);
        if (!$organization) {
            return $this->response->errorNotFound('Organization not found');
        }

        // Attach users to the organization
        $users_id = explode(',', $users_id);
        $organization->users()->attach($users_id);

        return $this->response->noContent();
    }

    /**
     * Create a new organization
     *
     * @param Request $request Request data
     * @return json
     * @throws StoreResourceFailedException
     */
    public function create(Request $request)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), Organization::rules());
        if ($validator->fails()) {
            throw new StoreResourceFailedException('Could not create new organization.', $validator->errors());
        }

        // Create the organization
        $organization = Organization::create([
            'name'  => $request->get('name'),
            'email' => $request->get('email')
        ]);

        return $this->response->item($organization, new OrganizationTransformer)->setStatusCode(201);
    }

    /**
     * Remove users from an organization
     *
     * @param Request $request Request data
     * @param string $organization_id Organization's identifier
     * @param string $users_id Identifiers
     * @return json
     */
    public function detachUsers(Request $request, string $organization_id, string $users_id)
    {
        // Find user
        $organization = Organization::find($organization_id);
        if (!$organization) {
            return $this->response->errorNotFound('Organization not found');
        }

        // Attach users to the organization
        $users_id = explode(',', $users_id);
        $organization->users()->detach($users_id);

        return $this->response->noContent();
    }

    /**
     * Remove organizations with the given identifiers
     *
     * @param string|int $organizations_id Identifiers
     * @return int Organization count removed
     */
    public function remove(string $organizations_id)
    {
        return Organization::destroy(explode(',', $organizations_id));
    }

    /**
     * Retrieve organizations with the given identifiers
     *
     * @param string|int $organizations_id Identifiers
     * @return json
     */
    public function show(string $organizations_id)
    {
        $organizations_id = explode(',', $organizations_id);
        $organizations = Organization::whereIn('id', $organizations_id)->get();

        if (count($organizations) != count($organizations_id))
        {
            // Retrieve missing identifiers to notify the organization
            $result_ids = collect($organizations)->pluck('id')->toArray();
            $diffs = array_diff($organizations_id, $result_ids);

            return $this->response->errorNotFound('Organization(s) not found (Id: '.implode(', ', $diffs).')');
        }

        return $this->response->collection($organizations, new OrganizationTransformer);
    }

    /**
     * Retrieve user's organization
     *
     * @param string $user_id Owner identifier
     * @return json
     */
    public function showForUser(string $user_id)
    {
        $organizations = Organization::whereHas('users', function($query) use($user_id)
        {
            $query->where('id', '=', $user_id);
        })->get();

        return $this->response->collection($organizations, new OrganizationTransformer);
    }

    /**
     * Update the given organization
     *
     * @param Request $request Request data
     * @param string $organization_id Identifier
     * @return json
     * @throws UpdateResourceFailedException
     */
    public function update(Request $request, string $organization_id)
    {
        // Validate data
        $validator = app('validator')->make($request->all(), Organization::rules());
        if ($validator->fails()) {
            throw new UpdateResourceFailedException('Could not update organization.', $validator->errors());
        }

        // Update data
        $organization = Organization::findOrFail($organization_id)->first();
        $organization->name = $request->has('name') ? $request->get('name') : $organization->name;
        $organization->description = $request->has('description') ? $request->get('description') : $organization->description;
        $organization->email = $request->has('email') ? $request->get('email') : $organization->email;
        $organization->website = $request->has('website') ? $request->get('website') : $organization->website;
        $organization->country = $request->has('country') ? $request->get('country') : $organization->country;
        $organization->save();

        return $this->response->item($organization, new OrganizationTransformer);
    }
}
