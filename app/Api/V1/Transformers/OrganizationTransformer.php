<?php

namespace App\Api\V1\Transformers;

use App\Models\Organization;
use League\Fractal\TransformerAbstract;

class OrganizationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'products',
        'users'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Organization $organization)
    {
        return [
            'id'            => (int) $organization->id,
            'name'          => $organization->name,
            'description'   => $organization->description,
            'email'         => $organization->email,
            'website'       => $organization->website,
            'country'       => $organization->country,
            'links'         => [
                [
                    'rel' => 'self',
                    'uri' => '/organizations/'.$organization->id,
                ]
            ]
        ];
    }

    /**
     * Include Product
     *
     * @return League\Fractal\ItemResource
     */
    public function includeProducts(Organization $organization)
    {
        return $this->collection($organization->products, new ProductTransformer);
    }

    /**
     * Include User
     *
     * @return League\Fractal\ItemResource
     */
    public function includeUsers(Organization $organization)
    {
        return $this->collection($organization->users, new UserTransformer);
    }
}
