<?php

namespace App\Api\V1\Transformers;

use App\Models\Product;
use League\Fractal\TransformerAbstract;

class ProductTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id'            => (int) $product->id,
            'name'          => $product->name,
            'description'   => $product->description,
            'website'       => $product->website,
            'links'         => [
                [
                    'rel' => 'self',
                    'uri' => '/products/'.$product->id,
                ]
            ]
        ];
    }
}
