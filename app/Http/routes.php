<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

    // Public
    $api->get('auth/ask-token/{user_email}', 'App\Api\V1\Controllers\AuthenticateController@askToken');
    $api->get('auth/use-token/{token}', 'App\Api\V1\Controllers\AuthenticateController@useToken');
    $api->get('auth/refresh-token/{token}', 'App\Api\V1\Controllers\AuthenticateController@refreshToken');

    // Routes who need an account
    $api->group(['middleware' => 'auth:api'], function($api)
    {
        $api->get('organizations', 'App\Api\V1\Controllers\OrganizationsController@all');
        $api->get('products', 'App\Api\V1\Controllers\ProductsController@all');
        $api->get('products/{products_id}', 'App\Api\V1\Controllers\ProductsController@show');
        $api->patch('products/{product_id}', 'App\Api\V1\Controllers\ProductsController@update');
        $api->delete('products/{products_id}', 'App\Api\V1\Controllers\ProductsController@remove');
        $api->get('users', 'App\Api\V1\Controllers\UsersController@all');
        $api->get('users/search', 'App\Api\V1\Controllers\UsersController@search');
        $api->post('users', 'App\Api\V1\Controllers\UsersController@create');

        // Users
        $api->get('users/{users_id}', 'App\Api\V1\Controllers\UsersController@show');
        $api->get('users/{user_id}/organizations', 'App\Api\V1\Controllers\OrganizationsController@showForUser');
        $api->patch('users/{user_id}', 'App\Api\V1\Controllers\UsersController@update');
        $api->delete('users/{users_id}', 'App\Api\V1\Controllers\UsersController@remove');

        // Organization
        $api->get('organizations/{organizations_id}', 'App\Api\V1\Controllers\OrganizationsController@show');
        $api->post('organizations', 'App\Api\V1\Controllers\OrganizationsController@create');
        $api->patch('organizations/{organization_id}', 'App\Api\V1\Controllers\OrganizationsController@update');
        $api->delete('organizations/{organizations_id}', 'App\Api\V1\Controllers\OrganizationsController@remove');

        // Organization's products
        $api->get('organizations/{organization_id}/products', 'App\Api\V1\Controllers\ProductsController@showForOrganization');
        $api->post('organizations/{organization_id}/products', 'App\Api\V1\Controllers\ProductsController@create');

        // Organization's users
        $api->get('organizations/{organization_id}/users', 'App\Api\V1\Controllers\UsersController@showForOrganization');
        $api->put('organizations/{organization_id}/users/{users_id}', 'App\Api\V1\Controllers\OrganizationsController@attachUsers');
        $api->delete('organizations/{organization_id}/users/{users_id}', 'App\Api\V1\Controllers\OrganizationsController@detachUsers');
    });
});
