<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Organization extends Model implements Ownable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'email', 'website', 'country'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get all products owned by this organization.
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * Get all users in this organization.
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Get owners of the resource
     *
     * @return array
     */
    public function getOwnersKeys() : array
    {
        return [];
    }

    /**
     * Attributes rules
     *
     * @param string $id Identifier to avoid unique verification on update
     * @return array
     */
    static public function rules(string $id = '')
    {
        return [
            'name'          => 'required|min:3|max:80|string',
            'description'   => 'string',
            'email'         => 'email',
            'website'       => 'url',
            'country'       => 'string'
        ];
    }
}
