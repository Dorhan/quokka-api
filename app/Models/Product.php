<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model implements Ownable
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'website', 'organization_id'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get organization owning this product.
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * Get owners of the resource
     *
     * @return array
     */
    public function getOwnersKeys() : array
    {
        return [];
    }

    /**
     * Attributes rules
     *
     * @param string $id Identifier to avoid unique verification on update
     * @return array
     */
    static public function rules(string $id = '')
    {
        return [
            'name'          => 'min:3|max:80',
            'description'   => 'string',
            'website'       => 'url'
        ];
    }
}
