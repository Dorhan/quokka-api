<?php

namespace App\Models;

interface Ownable
{
    /**
    * Get owners of the resource
    *
    * @return array
    */
    public function getOwnersKeys() : array;
}