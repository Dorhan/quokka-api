<?php

namespace App\Models;

use Carbon\Carbon;
use DB;
use Illuminate\Database\Eloquent\Model;

class UserToken extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users_tokens';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'token'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * Get all organizations where user exists.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get active tokens
     *
     * @param mixed $query Laravel's query data
     * @param int seconds Maximum life time to be considered has active (default: 15 minutes)
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query, int $seconds = 900)
    {
        return $query->where('updated_at', '>=', Carbon::now()->subSecond($seconds));
    }

    /**
     * Generate a random token
     *
     * @param int $length Length asked
     * @return string
     */
    static public function generateToken(int $length = 120) : string
    {
        return bin2hex(random_bytes($length / 2));
    }
}
