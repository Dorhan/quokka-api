<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Lumen\Auth\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements JWTSubject, AuthenticatableContract, AuthorizableContract
{
    use SoftDeletes, Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['email', 'name'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Get all organizations.
     */
    public function organizations()
    {
        return $this->belongsToMany(Organization::class);
    }

    /**
     * Get all tokens.
     */
    public function tokens()
    {
        return $this->hasMany(UserToken::class)->active();
    }

    /**
     * Retrieve identifier
     *
     * @return string
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Retrieve custom JWT attributes
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * Attributes rules
     *
     * @param string $id Identifier to avoid unique verification on update
     * @return array
     */
    static public function rules(string $id = '')
    {
        return [
            'email' => 'required|email|unique:users,email,'.$id,
            'name' => 'min:3|max:80'
        ];
    }
}
