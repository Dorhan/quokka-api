<?php

namespace App\Events;

class UserCreatedEvent extends Event
{
    /**
     * User instance
     *
     * @var User
     */
    public $user = null;

    /**
     * Create a new event instance.
     *
     * @param User $user A User instance
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
