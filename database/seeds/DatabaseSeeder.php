<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Organization::class, 30)
            ->create()
            ->each(function($o) {

                for ($i = 0; $i < rand(1, 12); $i++) 
                    $o->users()->save(factory(App\Models\User::class)->make());

                for ($i = 0; $i < rand(0, 5); $i++) 
                    $o->products()->save(factory(App\Models\Product::class)->make());
        });
    }
}
