<?php


$factory->define(App\Models\User::class, function ($faker) {
    return [
        'email' => $faker->email,
        'name' => $faker->name,
    ];
});
