<?php

$factory->define(App\Models\Organization::class, function ($faker) {
    return [
        'name' => $faker->company,
        'description' => $faker->catchPhrase,
        'email' => $faker->companyEmail,
        'website' => $faker->domainName,
        'country' => $faker->country
    ];
});
