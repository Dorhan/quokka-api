<?php

$factory->define(App\Models\Product::class, function ($faker) {
    return [
        'name' => $faker->domainWord,
        'description' => $faker->catchPhrase,
        'website' => $faker->domainName
    ];
});
